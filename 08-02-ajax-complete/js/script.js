$(document).ready(function(){
    $("#example").on("click","button.switch", function(){

        $.ajax("http://jquery-19-1.herokuapp.com/result.html", {
            beforeSend: function(){
                $("#status").text("Carregando...");
            }
        })

        .done(function(response){
            $("#result").html(response);
        })

        .fail(function(request,errorType,errorMessage){
            //timeout, error, abort, parserror
        })

        .always(function() {
            $("#status").text("Completo!");
        });

    });
})